## I. Práva

1. Hráč musí dodržovat zákony České a Slovenské republiky.
2. Neznalost pravidel neomlouvá.
3. Hráč je povinen respektovat MC A-T a především správce.
4. Pokud hráč podává stížnost na člena A-T, Správce nebo hráče, který porušuje pravidla, musí předložit platné důkazy.
5. Hráč má právo na vlastní názor.
6. Není zde tolerován rasismus, urážky, pomluvy, nacismus a jiné.
7. Správce jakožto vlastník má právo kohokoliv ze serveru bez udání důvodu zabanovat, či mu jinak omezit přístup v případě, že daný hráč aktivně porušuje pravidla.
8. Hráč je zodpovědný za svůj účet. V případě krádeže účtu není chyba na naší straně nýbrž na straně hráče.
9. Po neaktivitě hráče větší jak 14 dnů, která nebyla ohlášena, propadá všechen hráčův majetek.
10. Po neaktivitě hráče větší jak 30 dnů která nebyla ohlášena bude hráčův účet smazán!

## II. Zákazy

1. Je zakázáno zneužívat a využívat bugů serveru nebo hry.
2. Je zakázáno hledat chyby pravidlech.
3. Je zakázáno používání cheat a dalších klient módů, které zvýhodňují hráče.
4. Hráč nesmí požadovat itemy od administrátorů.
5. Je zakázáno nadávat, urážet či provokovat ostatní a spamovat/floodovat v chatu.
6. Je zakázáno sdílet osobní informace o hráčích.
7. Je zakázáno obcházet udělené tresty.
8. Je zakázáno jakkoliv griefovat server. - Všechny provedené akce jsou řádně logované.
9. PvP je povoleno pouze se souhlasem druhého hráče, pokud se nebudou chtít PvP účastnit, je zakázáno na ně útočit.
10. Je zakázáno stavět stavby, redstone obvody či jiné věci, které budou lagovat server.
11. Je zakázáno jakýmkoliv způsobem šířit reklamu na jiný server, web, či jiné odkazy.

**Platnost pravidel od 17.11.2020**

_Vyhrazujeme si právo na změnu pravidel bez upozornění._
_Po porušení jakéhokoliv pravidla bude daný člověk potrestán._